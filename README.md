# Puzzle Game Using JavaScript

    Simple Puzzle Game Using Plane JavaScript.

## Getting started

    ⚫ Start puzzle game
<img src="./images/start puzzle.png" alt="Restorers Bill">

    ⚫ Winner puzzle game
<img src="./images/winner puzzle.png" alt="Restorers Bill">

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
